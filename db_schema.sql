-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: amarta_delivery
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_delivery_item`
--

DROP TABLE IF EXISTS `t_delivery_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_delivery_item` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_delivery_order_id` int(11) DEFAULT NULL,
  `no_spk` varchar(255) DEFAULT NULL,
  `spk_date` varchar(255) DEFAULT NULL,
  `no_sj` varchar(255) DEFAULT NULL,
  `sj_date` varchar(255) DEFAULT NULL,
  `konsumen_name` varchar(255) DEFAULT NULL,
  `konsumen_adrs` varchar(255) DEFAULT NULL,
  `konsumen_telp` varchar(255) DEFAULT NULL,
  `id_tipe_motor` varchar(255) DEFAULT NULL,
  `jenis_motor` varchar(255) DEFAULT NULL,
  `warna_motor` varchar(255) DEFAULT NULL,
  `no_mesin` varchar(255) DEFAULT NULL,
  `no_rangka` varchar(255) DEFAULT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `location` point DEFAULT NULL,
  `autograph` mediumblob,
  `db_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `autograph_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `fk_t_delivery_item_idx` (`_delivery_order_id`),
  CONSTRAINT `fk_t_delivery_item` FOREIGN KEY (`_delivery_order_id`) REFERENCES `t_delivery_order` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_delivery_order`
--

DROP TABLE IF EXISTS `t_delivery_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_delivery_order` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_do` varchar(255) DEFAULT NULL,
  `do_date` varchar(255) DEFAULT NULL,
  `id_dealer` varchar(255) DEFAULT NULL,
  `dealer_name` varchar(255) DEFAULT NULL,
  `driver_name` varchar(255) DEFAULT NULL,
  `no_p` varchar(255) DEFAULT NULL,
  `shift` varchar(255) DEFAULT NULL,
  `db_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deletion_flat` int(11) DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_photo`
--

DROP TABLE IF EXISTS `t_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_photo` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_delivery_item_id` int(11) DEFAULT NULL,
  `binary` mediumblob,
  `location` point DEFAULT NULL,
  `db_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `photo_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `fk_delivery_item_idx` (`_delivery_item_id`),
  CONSTRAINT `fk_delivery_item` FOREIGN KEY (`_delivery_item_id`) REFERENCES `t_delivery_item` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-04 10:25:28
