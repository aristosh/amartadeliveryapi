var mysql = require('mysql');
var express = require('express');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var im = require('imagemagick');
var multipartMiddleware = multipart();
var fs = require('fs');
var app = express();

var PORT = process.env.PORT || 8000;
var LIST_PER_PAGE = 10;
var MESSAGE_SUCCESS = 'success';
var MESSAGE_ERROR = 'error';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// allow cross domain ajax request
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// production server
var connection = mysql.createConnection({
  //host     : '192.168.0.53',
  host     : 'localhost',
  user     : 'root',
  password : 'z742096001',
  database : 'amarta_delivery'
});

// development server
/*var connection = mysql.createConnection({
  host     : '192.168.56.101',
  user     : 'admin',
  password : 'Lembang1',
  database : 'amarta_delivery'
});*/

connection.connect(function(err) {
  if (err) {
    console.error('error connecting to database');
    process.exit(1);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});

app.get('/api', function(req, res) {
  console.log('Test Client connected to Server');
  res.send('Connected');
});

/*
 * @resource
 *   delivery_order
 *
 * @method
 *   GET
 *
 * @query
 *   no_do
 *   no_sj
 */
app.get('/api/delivery_order', function(req, res) {
  var ret = {};
  var query_delivery_order = 'select * from t_delivery_order where no_do = \'' + req.query.no_do + '\' order by _id desc';
  connection.query(query_delivery_order,  function(err, orders, fields) {
    if(err) {
      res.send({msg : MESSAGE_ERROR, err : err});
      console.log(err);
      return;
    }

    if(orders.length == 0) {
      res.send({msg : MESSAGE_ERROR, err : 'empty record'});
      return;
    }
    var order = orders[0];
    ret = JSON.parse( JSON.stringify( order ) );
    var query_delivery_item = 'select _id, no_spk, spk_date, no_sj, sj_date, konsumen_name, konsumen_adrs, konsumen_telp, id_tipe_motor, ' + 
      'jenis_motor, warna_motor, no_mesin, no_rangka, tahun, location, db_timestamp, autograph_timestamp from t_delivery_item ' + 
      'where _delivery_order_id = \'' + order._id + '\'';

    if(req.query.no_sj)
      query_delivery_item += ' and no_sj = \'' + req.query.no_sj + '\'';

    connection.query(query_delivery_item, function(err, items, fields) {
      if(err) {
        res.send({msg : MESSAGE_ERROR, err : err});
        console.log(err);
        return;
      }

      if(items.length == 0) {
        res.send({msg : MESSAGE_ERROR, err : 'empty record'});
        return;
      }

      ret['items'] = JSON.parse( JSON.stringify(items) );

      var itemCount = ret.items.length;  // handle async
      for(var i in ret.items) {
        ret.items[i].autograph = 'http://' + req.headers.host + '/api/delivery_item/' + ret.items[i]._id + '/autograph';

        var query_photo = 'select _id, location, db_timestamp, photo_timestamp from t_photo where _delivery_item_id = ?';
        var _id = ret.items[i]._id;
        delete ret.items[i]._id;
        connection.query(query_photo, _id, (function(i) {
          return function(err, photos, fields) {
            ret.items[i]['photos'] = [];
            for(var j in photos) {
              var photo = JSON.parse( JSON.stringify(photos[j]) );
              photo.url = 'http://' + req.headers.host + '/api/photo/' + photo._id;
              delete photo._id;

              ret.items[i]['photos'].push(photo);
            }

            // handle async
            itemCount--;
            if(itemCount == 0)
              res.send({msg : MESSAGE_SUCCESS, result : ret});
          }
        })(i));
      }
    });
  });
});

/*
 * @resource
 *   delivery_order
 *
 * @method
 *   POST
 *
 * @body
 *   
 */
app.post('/api/delivery_order', function(req, res) {
  var record = JSON.parse( JSON.stringify( req.body ) );
  var query = 'insert into t_delivery_order set ?';

  connection.query(query, record, function(err, result) {
    if(err) {
      res.send({'msg' : MESSAGE_ERROR, 'err' : err});
      console.log(err);
      return;
    }
    res.send({'msg' : MESSAGE_SUCCESS, 'id' : result.insertId});
  });
});

app.delete('/api/delivery_order/:_id', function(req, res) {
  var query = 'delete from t_delivery_order where _id = ?'
  connection.query(query, req.params._id, function(err, result) {
    if(err) {
      res.send({msg : MESSAGE_ERROR, err : err});
      console.log(err);
      return;
    }
    res.send({msg : MESSAGE_SUCCESS});
  });
});

/*
 * @resource
 *   delivery_item/autograph
 *
 * @method
 *   GET
 *
 * @param
 *   delivery item id
 */
app.get('/api/delivery_item/:_id/autograph', function(req, res) {
  var query = 'select autograph from t_delivery_item where ?';
  connection.query(query, req.params, function(err, row, field) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      console.log(err);
      return;
    }

    if(!req.query.height && !req.query.width) {
      res.contentType('image/jpeg');
      res.send(row[0].autograph);
      return;
    } else {
      im.resize({format : 'jpeg', srcData : row[0].autograph, width : req.query.width, height : req.query.height}, function(resizeErr, resizeStdout, resizeStderr) {
        if(resizeErr) {
          res.send({msg : MESSAGE_ERROR, err : resizeErr});
          console.log(err);
          return;
        }
        res.contentType('image/jpeg')
        res.end(resizeStdout, 'autograph');
      });
    }
  });
});


/*
 * @resource
 *   delivery_item
 *
 * @method
 *   POST
 *
 * @body
 *   _delivery_order_id
 *   location : {lat, lng}
 * 
 * @file
 *   autograph
 */
app.post('/api/delivery_item', multipartMiddleware, function(req, res) {
  var binImage = fs.readFileSync(req.files.file.path);
  var record = JSON.parse( JSON.stringify( req.body ) );
  record.autograph = binImage; 
  delete record.lat;
  delete record.lng;
  
  var query = 'insert into t_delivery_item set ?, `location` = GeomFromText(\'POINT('+ req.body.lat + ' ' + req.body.lng + ')\')'; 
  var q = connection.query(query, record, function(err, result) {
    fs.unlink(req.files.file.path, function(err) {});

    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }
    console.log(result);
    res.send({'msg' : MESSAGE_SUCCESS, 'id' : result.insertId});
  });
  console.log(q);
});

/*
 * @resource
 *   photo 
 *
 * @method
 *   GET
 *
 * @param
 *   photo id
 */
app.get('/api/photo/:_id', function(req, res) {
  var query = 'select `binary` from t_photo where ?';
  connection.query(query, req.params, function(err, row, field) {
    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    if(!req.query.height && !req.query.width) {
      res.contentType('image/jpeg');
      res.send(row[0].binary);
      return;
    } else {
      im.resize({format : 'jpeg', srcData : row[0].binary, width : req.query.width, height : req.query.height}, function(resizeErr, resizeStdout, resizeStderr) {
        if(resizeErr) {
          console.log(JSON.stringify(resizeErr));
          res.send({msg : MESSAGE_ERROR, err : resizeErr});
          return;
        }
        res.contentType('image/jpeg')
        res.end(resizeStdout, 'binary');
      });
    }
  });
});

/*
 * @method
 *   POST
 *
 * @resource
 *   photo
 *
 * @description
 *   save a photo to database
 * 
 * @body
 *   _delivery_item_id  : fk to delivery item
 *
 * @files
 *   photo to be uploaded
 */
app.post('/api/photo', multipartMiddleware, function(req, res){
  var binImage = fs.readFileSync(req.files.file.path);
  var record = JSON.parse( JSON.stringify(req.body) );
  record.binary = binImage;
  delete record.lat;
  delete record.lng;

  connection.query('insert into t_photo set ?,  `location` = GeomFromText(\'POINT('+ req.body.lat + ' ' + req.body.lng + ')\')', record, function(err, row) {
    fs.unlink(req.files.file.path, function(err) {});

    if(err) {
      console.log(err);
      res.send({msg : MESSAGE_ERROR, err : err});
      return;
    }

    res.send({msg : MESSAGE_SUCCESS});
  });
});

console.log('listening on port ' + PORT + ' ...' )
app.listen(PORT)
